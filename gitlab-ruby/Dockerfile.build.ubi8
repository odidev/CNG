ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG RUBY_VERSION=2.7.5
ARG RUBYGEMS_VERSION=3.2.33
ARG BUNDLER_VERSION=2.3.15
ARG RBREADLINE_VERSION=0.5.5
ARG JEMALLOC_VERSION=5.2.1

ENV LANG=C.UTF-8
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

COPY shared/build-scripts/ /build-scripts
COPY patches/ /patches

RUN mkdir /assets \
    && curl --retry 6 -L -sfo jemalloc.tar.bz2 https://github.com/jemalloc/jemalloc/releases/download/${JEMALLOC_VERSION}/jemalloc-${JEMALLOC_VERSION}.tar.bz2 \
    && tar -xjf jemalloc.tar.bz2 \
    && cd jemalloc-${JEMALLOC_VERSION} \
    && ./autogen.sh --prefix=/usr --libdir=${LIBDIR} --enable-prof \
    && make -j "$(nproc)" install \
    && cd .. \
    && export RUBY_MAJOR_VERSION="${RUBY_VERSION%.*}" \
    && curl -f --retry 6 -s https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR_VERSION}/ruby-${RUBY_VERSION}.tar.gz | tar -xz \
    && cd ruby-${RUBY_VERSION} \
    && patch -p1 < /patches/thread-memory-allocations-${RUBY_MAJOR_VERSION}.patch \
    && export LDFLAGS="-Wl,--no-as-needed" \
    && cflags="-fno-omit-frame-pointer" ./configure --prefix=/usr --libdir=${LIBDIR} --with-jemalloc --disable-dtrace --disable-install-doc --disable-install-rdoc --enable-shared --with-out-ext=dbm,readline --without-gmp --without-gdbm --without-tk \
    && make -j "$(nproc)" install \
    && gem update --no-document --system ${RUBYGEMS_VERSION} \
    && gem install bundler --version ${BUNDLER_VERSION} --force --no-document \
    && cd .. \
    && curl --retry 6 -sfL https://github.com/connoratherton/rb-readline/archive/v${RBREADLINE_VERSION}.tar.gz | tar -xz \
    && ruby rb-readline-${RBREADLINE_VERSION}/setup.rb \
    && /build-scripts/cleanup-gems ${LIBDIR}/ruby/gems \
    && cp -R --parents \
      /usr/bin/{ruby,rdoc,irb,erb,rake,gem,bundler,bundle} \
      ${LIBDIR}/{ruby/,libruby.*,libjemalloc.*} \
      /usr/include/{ruby-*,jemalloc}/ \
      /assets
