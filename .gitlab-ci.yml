include:
  - ci_files/variables.yml
  - ci_files/final_images.yml
  # common if and rules definitions
  - local: .gitlab/ci/rules.gitlab-ci.yml
  # common templates
  - local: .gitlab/ci/common.gitlab-ci.yml
  # dependencies.io job
  - local: .gitlab/ci/deps.gitlab-ci.yml
    rules:
      - if: '$DEPS_PIPELINE == "true"'
  # build container images
  - local: .gitlab/ci/images.gitlab-ci.yml
    rules:
      - if: '$DEPS_PIPELINE != "true"'
  - template: Security/Dependency-Scanning.gitlab-ci.yml
    rules:
      - if: '$DEPS_PIPELINE != "true"'

variables:
  # Ensures that existing file permissions are respected when GitLab Runner clones the project.
  FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR: "true"
  # Format of the auto-deploy tag for auto-deploy builds.
  # https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/auto-deploy.md#auto-deploy-tagging
  AUTO_DEPLOY_TAG_REGEX: '^[0-9]+\.[0-9]+\.[0-9]+\+[a-z0-9]{7,}$'
  AUTO_DEPLOY_BRANCH_REGEX: '^[0-9]+-[0-9]+-auto-deploy-[0-9]+$'
  # Suffix to the cache key of gitlab-rails-* where Gem vendor bundles are stored. Enables easy cache busting.
  RAILS_CACHE_SUFFIX: "20220118" # quoted string, so `gitlab-runner exec ...` functions
  DNF_OPTS: '--disableplugin=subscription-manager'
  DOCKER_VERSION: 20.10.15
  ISSUE_BOT_LABELS_EXTRA: "group::distribution"
  # Version for all instances of `gitlab-omnibus-builder/ruby_docker`
  BUILDER_RUBY_DOCKER: "3.1.0"

stages:
  - test
  - prepare
  - prepare:phase-one
  - prepare:phase-two
  - prepare:phase-three
  - prepare:phase-four
  - phase-zero
  - phase-one
  - phase-two
  - phase-three
  - phase-four
  - phase-five
  - phase-six
  - final-list
  - release
  - container-scanning
  - report

workflow:
  rules:
    # No pipeline on auto-deploy branches as a tag will definitely follow
    - if: '$CI_COMMIT_BRANCH =~ /^[0-9]+-[0-9]+-auto-deploy-[0-9]+$/'
      when: never
    # For all other branches, create a pipeline. We are explicitly specifying
    # this so that this rule gets matched earlier before MR pipelines gets
    # triggered, thus causing two pipelines for a branch push - a regular one
    # and a detached one. If we ever decide not to run pipelines on branch
    # pushes that doesn't cause an MR, we can change the following to
    # $CI_MERGE_REQUEST_IID
    - if: '$CI_COMMIT_BRANCH'
    # No pipeline on tag pushes to com. We build them in dev and sync to com
    - if: '$CI_PROJECT_PATH == "gitlab-org/build/CNG" && $CI_COMMIT_TAG'
      when: never
    # For all other tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

issue-bot:
  stage: report
  image: registry.gitlab.com/gitlab-org/distribution/issue-bot:latest
  script: /issue-bot
  rules:
    # Only run when pipeline fails for the default branch for the primary project on gitlab.com
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_SERVER_HOST == "gitlab.com" && $CI_PROJECT_PATH == "gitlab-org/build/CNG"'
      when: on_failure

danger-review:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:danger
  stage: prepare
  cache: {}
  rules:
    # source not a push, this is not a tag, not the default or stable branches
    - if: '$CI_PIPELINE_SOURCE != "push" || $CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH =~ /^[0-9]+-[0-9]+-stable$/'
      when: never
    # unless above, if API token defined, run Danger
    - if: '$DANGER_GITLAB_API_TOKEN'
  script:
    - danger --fail-on-errors=true
